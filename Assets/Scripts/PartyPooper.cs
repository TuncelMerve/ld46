﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PartyPooper : InteractableObject
{
	public override void Awake()
	{
		base.Awake();

		//set random reward and penalty
		Penalty = Random.Range(-1, 2);
		Penalty = (int)Mathf.Clamp(Penalty, Single.NegativeInfinity, 0);

		if (Penalty < 0)
		{
			Score = 5;
			
		}
		else
		{
			Score = -2;
		}
	}

	public override void OnMouseDown()
	{
		PlaySound();
		AddScore(Score);
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
	}

	public void ShowCharacter()
	{
		gameObject.GetComponent<SpriteRenderer>().enabled = true;
		InvokeRepeating("DecreaseScore", 2.0f, 1f);
	}

	private void DecreaseScore()
	{
		AddScore(Penalty);
	}
}
