﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coat : InteractableObject
{
	public bool IsTidy;

	[SerializeField] private int _mCoatId;

	[SerializeField] private List<Transform> _mHangerPositions;

	public override void Awake()
	{
		base.Awake();
	}

	public override void OnMouseDown()
	{	
		if (!IsTidy)
		{
			PlaySound();
			HangCoat();
		}
	}

	private void HangCoat()
	{
		IsTidy = true;
		AddScore(Score);

		//set position to the hanger
		transform.localRotation = new Quaternion(0,0,0,0);
		transform.localPosition = _mHangerPositions[_mCoatId].position;
	}
}
