﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloons : InteractableObject
{
	[SerializeField] private AudioSource _mPopSound;

	public override void Awake()
	{
		base.Awake();
	}

	public override void OnMouseDown()
	{
		//chance to pop
		var selection = Choose(new float[]
		{
			30f, 70f
		});
		var shouldPop = selection == 0;

		if (shouldPop)
		{
			_mPopSound.Play();
			DecreaseScore(Penalty);
		}
		else
		{
			PlaySound();
			AddScore(Score);
		}


	}

	private float Choose(float[] probs)
	{
		float total = 0;

		foreach (var elem in probs)
		{
			total += elem;
		}

		float randomPoint = Random.value * total;

		for (int i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
			{
				return i;
			}
			else
			{
				randomPoint -= probs[i];
			}
		}
		return probs.Length - 1;
	}



}
