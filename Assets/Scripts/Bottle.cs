﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottle : InteractableObject
{
	public override void Awake()
	{
		base.Awake();
	}

	public override void OnMouseDown()
	{
		//chance to add
		var selection = Choose(new float[]
		{
			50f, 50f
		});
		var shouldGive = selection == 0;

		if (shouldGive)
		{
			PlaySound();
			AddScore(Score);		
		}
		else
		{
			PlaySound();
			DecreaseScore(Penalty);
		}
	}

	private float Choose(float[] probs)
	{
		float total = 0;

		foreach (var elem in probs)
		{
			total += elem;
		}

		float randomPoint = Random.value * total;

		for (int i = 0; i < probs.Length; i++)
		{
			if (randomPoint < probs[i])
			{
				return i;
			}
			else
			{
				randomPoint -= probs[i];
			}
		}
		return probs.Length - 1;
	}

}
