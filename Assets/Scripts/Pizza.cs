﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pizza : InteractableObject
{
	public override void Awake()
	{
		base.Awake();
	}

	public override void OnMouseDown()
	{
		base.OnMouseDown();

		PlaySound();
		AddScore(Score);
	}


}
