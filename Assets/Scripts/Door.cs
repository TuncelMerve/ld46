﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : InteractableObject
{

	[SerializeField] private List<Coat> _mCoats;
	[SerializeField] private List<PartyPooper> _mCharacters;

	private int _mCoatSpawnId;
	private bool _mIsRinging;

	public override void Awake()
	{
		base.Awake();

		_mCoatSpawnId = 0;

		InvokeRepeating("RingDoor", 2.0f, 10f);
	}

	public override void OnMouseDown()
	{
		//check if the door is ringing
		if (!_mIsRinging) return;

		StopSound();
		_mIsRinging = false;
		AddScore(Score);

		SpawnCoat();
	}

	private void SpawnCoat()
	{
		if (_mCoats.Count == _mCoatSpawnId) return;
		_mCoats[_mCoatSpawnId].gameObject.SetActive(true);
		_mCharacters[_mCoatSpawnId].ShowCharacter();
		_mCoatSpawnId++;
	}

	private void RingDoor()
	{
		PlaySound();
		_mIsRinging = true;
	}
}
