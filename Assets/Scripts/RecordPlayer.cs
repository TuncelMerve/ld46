﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordPlayer : InteractableObject
{
	private bool _mIsPLaying;
	private float _mClicked;
	private float _mClickTime;
	private float _mClickDelay = 0.4f;

	public override void Awake()
	{
		base.Awake();
		_mIsPLaying = true;
	}

	public override void OnMouseDown()
	{
		if (_mIsPLaying)
		{
			_mClicked++;

			if (_mClicked == 1)
			{
				_mClickTime = Time.time;
				Invoke("StopMusicAttempt", 0.5f);
			}

			if (_mClicked > 1 && Time.time - _mClickTime < _mClickDelay)
			{
				_mClicked = 0;
				_mClickTime = 0;
				AddScore(Score);
				PlaySound();

			}
			//else if (Time.time - _mClickTime >= _mClickDelay )
			//{
			//	DecreaseScore(Penalty);
			//	StopMusic();
			//	_mIsPLaying = false;
			//	_mClicked = 0;				
			//}

		}
		else
		{			
			StartMusic();
			_mIsPLaying = true;
			_mClicked = 0;
		}

	}

	private void StopMusicAttempt()
	{
		if (_mIsPLaying && _mClickTime != 0 && Time.time - _mClickTime >= _mClickDelay)
		{
			DecreaseScore(Penalty);
			StopMusic();
			_mIsPLaying = false;
			_mClicked = 0;
		}
	}

}
