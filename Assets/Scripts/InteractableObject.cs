﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableObject : MonoBehaviour
{
	[SerializeField] private AudioSource _mAudioSource;
	public int Score;
	public int Penalty;

	private Player _mPlayer;

	public virtual void Awake()
	{
		_mPlayer = FindObjectOfType<Player>();
	}

	public virtual void AddScore(int score)
	{
		_mPlayer.AddScore(score);
	}

	public virtual void DecreaseScore(int score)
	{
		_mPlayer.AddScore(score);
	}

	public virtual void PlaySound()
	{
		_mAudioSource.Play();
	}

	public virtual void StopSound()
	{
		_mAudioSource.Stop();
	}

	public virtual void OnMouseDown()
	{
		HideObject();
	}

	public void StopMusic()
	{
		_mPlayer.StopMusic();
	}

	public void StartMusic()
	{
		_mPlayer.StartMusic();
	}

	public void HideObject()
	{
		gameObject.GetComponent<PolygonCollider2D>().enabled = false;
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		Invoke("ShowObject", 5f);
	}

	public void ShowObject()
	{
		gameObject.GetComponent<PolygonCollider2D>().enabled = true;
		gameObject.GetComponent<SpriteRenderer>().enabled = true;
	}
}
