﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	[SerializeField] private TMP_Text _mIntroText;
	[SerializeField] private TMP_Text _mScoreText;
	[SerializeField] private TMP_Text _mCountdownText;
	[SerializeField] private AudioSource _mAudioSource;
	[SerializeField] private GameObject _mIntroPanel;
	[SerializeField] private GameObject _mGameOverPanel;
	[SerializeField] private GameObject _mSuccessPanel;
	[SerializeField] private Button _mRestartButton;
	[SerializeField] private string _mMessage;

	public bool PartyStarted;

	private float _mScore;
	private int _mTimeLeft;
	private float _mLetterPause = 0.002f;

	public void Awake()
	{
		_mScore = 10;
		_mTimeLeft = 60;
		_mIntroText.text = "";
		StartCoroutine(TypeText());
		_mRestartButton.onClick.AddListener(Restart);
	}

	private void Restart()
	{
		SceneManager.LoadScene(0);
	}

	public void AddScore(int score)
	{
		_mScore += score;

		_mScore = Mathf.Clamp(_mScore, 0, Single.PositiveInfinity);

		//game over if _mScore is zero
		if (_mScore == 0)
		{
			_mGameOverPanel.SetActive(true);
			PartyStarted = false;
		}

		//_mScoreText.text = _mScore.ToString();

		if (_mScore < 20 && _mScore > 5)
		{
			_mScoreText.text = "MEH.";
		}
		else if (_mScore < 5)
		{
			_mScoreText.text = "AWKWARD.";
		}
		else if (_mScore >= 20)
		{
			_mScoreText.text = "AWESOME!";
		}
	}

	public void StopMusic()
	{
		_mAudioSource.Pause();
	}

	public void StartMusic()
	{
		_mAudioSource.UnPause();
	}

	private void LosePoint()
	{
		if(!PartyStarted) return;

		AddScore(-1);
		_mTimeLeft--;
		if (_mTimeLeft == 0)
		{
			if (_mScore >= 20)
			{
				_mSuccessPanel.SetActive(true);
				PartyStarted = false;
			}
			else
			{
				_mGameOverPanel.SetActive(true);
				PartyStarted = false;
			}

		}

		if (_mTimeLeft < 10)
		{
			_mCountdownText.text = string.Format("0:0" + _mTimeLeft);
		}
		else
		{
			_mCountdownText.text = string.Format("0:" + _mTimeLeft);
		}
		
	}

	IEnumerator TypeText()
	{
		foreach (char letter in _mMessage.ToCharArray())
		{
			_mIntroText.text += letter;
			yield return 0;
			yield return new WaitForSeconds(_mLetterPause);
		}

		yield return new WaitForSeconds(3);
		_mIntroPanel.SetActive(false);
		PartyStarted = true;
		InvokeRepeating("LosePoint", 1.0f, 1f);

	}
}
